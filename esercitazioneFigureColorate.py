import imutils, cv2

image = cv2.imread("Img1.jpg")
cv2.imshow("image", image)

blur = cv2.GaussianBlur(image,(99,99),5)
cv2.imshow("blur", blur)

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow("gray", gray)

edged = cv2.Canny(image, 20, 100)
cv2.imshow("edged", edged)

thresh = cv2.threshold(gray, 225, 255, cv2.THRESH_BINARY_INV)[1]
mask = thresh.copy()
mask = cv2.erode(mask, None, iterations = 5)

cv2.imshow("thresh", thresh)
cv2.imshow("mask", mask)


contours = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
contours = imutils.grab_contours(contours)

imcopy = image.copy()

for c in contours:
    cv2.drawContours(imcopy, [c], -1, (0, 0, 255), 2)


cv2.putText(imcopy, "Found {} objects".format(len(contours)), (20,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
cv2.imshow("contours", imcopy)

cv2.waitKey(0)