import imutils, cv2

image = cv2.imread("whiteString.jpg")
cv2.imshow("whiteImage", image)

#acquisisco le dimensioni dell'immagine per trovarne poi il centro
(h, w, d) = image.shape
print("width = {}, height = {} depth = {}".format(w, h, d))

#primo metodo per la rotazione
center = (w // 2, h // 2)
M = cv2.getRotationMatrix2D(center, -90, 1.0)
firstRotation = cv2.warpAffine(image, M, (w,h))
cv2.imshow("firstRotation", firstRotation)

#secondo metodo per la rotazione
secondRotation = imutils.rotate_bound(image, -45)
cv2.imshow("secondRotation", secondRotation)

#ritaglio immagine
partOfImage = image[50:200, 30:450]
cv2.imshow("partOfImage", partOfImage)

#Fattore di scala 
scale = 300.0 / w;
newSize = (300, int(h * scale))

resizedImage = cv2.resize(image, newSize)
cv2.imshow("resizedImage", resizedImage)

#disegno sull'immagine
resizedImage2 = cv2.resize(image, (400,400))
cv2.rectangle(resizedImage2, (300, 30), (400, 150), (0, 0, 255), 2)
cv2.circle(resizedImage2, (100, 100), 30, (0, 0, 255), -1)
cv2.line(resizedImage2, (200, 200), (400, 400), (0, 0, 255), 4)
cv2.putText(resizedImage2, "text", (20,20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
cv2.imshow("Rectangle", resizedImage2)

cv2.waitKey(0)